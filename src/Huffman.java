import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * https://docs.oracle.com/javase/7/docs/api/java/util/PriorityQueue.html
 * https://www.programiz.com/dsa/huffman-coding
 * https://enos.itcollege.ee/~jpoial/algoritmid/kodeerimisyl.html
 * https://algs4.cs.princeton.edu/55compression/Huffman.java.html
 * https://stackoverflow.com/questions/12310017/how-to-convert-a-byte-to-its-binary-string-representation
 */

/**
 * Prefix codes and Huffman tree.
 * Tree depends on source data.
 */
public class Huffman {
   HashMap<Byte, HuffmanNode> nodeLookupMap = new HashMap<>();
   HashMap<Byte, Integer> frequencyMap = new HashMap<>();
   ArrayList<HuffmanNode> encodedNodesList = new ArrayList<>();
   String originalText;
   char[] originalTextCharArray;

   class HuffmanNode {
      int frequency;
      byte originalByteValue;

      int hcodeLength;
      byte code;
      byte huffmanCode;
      String hcode;

      private int type = 0; // 0 = leaf, 1 = internal

      HuffmanNode left = null;
      HuffmanNode right = null;

      HuffmanNode(byte byteVal, int frequencyVal) {
         originalByteValue = byteVal;
         frequency = frequencyVal;
      }

      HuffmanNode(HuffmanNode leftNode, HuffmanNode rightNode) {
         if (leftNode != null && rightNode != null) {
            frequency = leftNode.frequency + rightNode.frequency;
         } else {
            frequency = leftNode.frequency;
         }
         left = leftNode;
         right = rightNode;
         type = 1;
      }

      boolean isLeaf() {
         return type == 0;
      }

      boolean isSameType(HuffmanNode o) {
         return type == o.type;
      }

      void createHuffmanCode(byte codeVal, int huffmanCodeLength) {
         code = codeVal;
         hcodeLength = huffmanCodeLength;
         String strCode = "0000000" + Integer.toString(code, 2);
         hcode = strCode.substring(strCode.length() - hcodeLength);
         if (hcode.length() != 0) {
            huffmanCode = (byte) Integer.parseInt(hcode, 2);
         }
      }

      @Override
      public String toString() {
         return "Symbol " + getCharByByte(originalByteValue)
                 + " code: " + originalByteValue
                 + " frequency: " + frequency
                 + " and hcodeLength " + hcodeLength + " bits"
                 + " hcode " + hcode;
      }
   }

   // For comparing the nodes
   class ImplementComparator implements Comparator<HuffmanNode> {
      public int compare(HuffmanNode x, HuffmanNode y) {
         return x.frequency - y.frequency;
      }
   }

   public Character getCharByByte(byte target) {
      for (int i = 0; i < originalTextCharArray.length; i++) {
         if (originalTextCharArray[i] == target) {
            return originalText.charAt(i);
         }
      }
      return null;
   }

   public void printCode(HuffmanNode root, byte code, int huffmanCodeLength) {
      if (root.isLeaf()) {
         root.createHuffmanCode(code, huffmanCodeLength);
         nodeLookupMap.put(root.originalByteValue, root);
         System.out.println(root);
      } else {
         if (root.left != null) {
            printCode(root.left, (byte) (code << 1), huffmanCodeLength + 1);
         }
         if (root.right != null) {
            printCode(root.right, (byte) ((code << 1) + 1), huffmanCodeLength + 1);
         }
      }

   }

   /** Constructor to build the Huffman code for a given bytearray.
    * @param original source data
    */
   Huffman (byte[] original) {

      originalText = new String(original, StandardCharsets.UTF_8);
      originalTextCharArray = originalText.toCharArray();

      for (byte originalByte : original) {
         if (frequencyMap.containsKey(originalByte)) {
            frequencyMap.put(originalByte, frequencyMap.get(originalByte) + 1);
            continue;
         }
         frequencyMap.put(originalByte, 1);
      }

      PriorityQueue<HuffmanNode> nodes = new PriorityQueue<>(frequencyMap.entrySet().size(), new ImplementComparator());

      // create leaf nodes
      for (Map.Entry<Byte, Integer> entry : frequencyMap.entrySet()) {
         HuffmanNode hn = new HuffmanNode(entry.getKey(), entry.getValue());
         nodes.add(hn);
      }

      // create internal nodes until there is two left in queue
      while (nodes.size() > 2) {

         HuffmanNode nodeA = nodes.poll();
         HuffmanNode nodeB = nodes.poll();

         if (nodeA.isSameType(nodeB)) {
            nodes.add(new HuffmanNode(nodeA, nodeB));
         } else {
            HuffmanNode nodeC = nodes.poll();
            if (nodeA.frequency + nodeC.frequency > nodeA.frequency + nodeB.frequency) {
               nodes.add(new HuffmanNode(nodeA, nodeB));
               nodes.add(nodeC);
            } else {
               nodes.add(new HuffmanNode(nodeA, nodeC));
               nodes.add(nodeB);
            }
         }
      }

      HuffmanNode root = new HuffmanNode(nodes.poll(), nodes.poll());

      printCode(root, (byte) 0, 0);

      System.out.println("---- DONE ----");
   }

   HuffmanNode findNodeByHuffmanCodeStrValue(String target) {
      for (Map.Entry<Byte, HuffmanNode> entry : nodeLookupMap.entrySet()) {
         if (entry.getValue().hcode.equals(target)) {
            return entry.getValue();
         }
      }
      return null;
   }

   /** Length of encoded data in bits.
    * @return number of bits
    */
   public int bitLength() {
      int output = 0;
      for (HuffmanNode node: encodedNodesList) {
         output += node.hcodeLength;
      }
      return output;
   }


   /** Encoding the byte array using this prefixcode.
    * @param origData original data
    * @return encoded data
    */
   public byte[] encode (byte [] origData) {
      ArrayList<Byte> output = new ArrayList<>();
      ArrayList<HuffmanNode> nodes = new ArrayList<>();
      String currentByteStr = "";
      for (int i = 0; i < origData.length; i++) {

         byte original = origData[i];
         HuffmanNode node = nodeLookupMap.get(original);
         if (node == null) {
            continue; // skipping without throwing exception =)
         }

         nodes.add(node);
         currentByteStr += node.hcode;
         if (currentByteStr.length() > 7) {

            String fullCurrentByteStr = currentByteStr.substring(0, 8);
            output.add((byte) Integer.parseInt(fullCurrentByteStr, 2));

            currentByteStr = currentByteStr.substring(8);
         }

      }
      // Supplement zeroes to last byte if needed
      output.add((byte) Integer.parseInt((currentByteStr + "00000000").substring(0, 8), 2));

      // save nodes
      encodedNodesList = nodes;

      byte[] encodedByteArray = new byte[output.size()];
      for (int i = 0; i < output.size(); i++) {
         encodedByteArray[i] = output.get(i);
      }

      return encodedByteArray;
   }

   /** Decoding the byte array using this prefixcode.
    * @param encodedData encoded data
    * @return decoded data (hopefully identical to original)
    */
   public byte[] decode (byte[] encodedData) {

      ArrayList<HuffmanNode> nodes = new ArrayList<>();
      String candidate = "";
      for (int i = 0; i < encodedData.length; i++) {
         byte b2 = encodedData[i];
         String strRepr = String.format("%8s", Integer.toBinaryString(b2 & 0xFF)).replace(' ', '0');

         for (int ii = 0; ii < strRepr.length(); ii++){
            char c = strRepr.charAt(ii);
            candidate += c;
            HuffmanNode match = findNodeByHuffmanCodeStrValue(candidate);
            if (match != null) {
               nodes.add(match);
               if (nodes.size() == encodedNodesList.size()) {
                  break;
               }
               candidate = "";
            }
         }
      }

      byte[] decodedArray = new byte[nodes.size()];
      for (int i = 0; i < nodes.size(); i++) {
         decodedArray[i] = nodes.get(i).originalByteValue;
      }

      return decodedArray;
   }

   /** Main method. */
   public static void main (String[] params) {
      String tekst = "AAAABC";
      byte[] orig = tekst.getBytes();
      Huffman huf = new Huffman (orig);
      byte[] kood = huf.encode (orig);
      byte[] orig2 = huf.decode (kood);
      // must be equal: orig, orig2
      System.out.println (Arrays.equals (orig, orig2));
      new Huffman (orig2);
      int lngth = huf.bitLength();
      System.out.println ("Length of encoded data in bits: " + lngth);
   }
}

